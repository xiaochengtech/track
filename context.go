/*
   Copyright 2020 XiaochengTech

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package track

import (
	"context"
)

// 内部记录TrackId时使用的Key值
const KEY_ID = "TRACKIDENTIFIER"

// 生成新的Track对象
func New() context.Context {
	c := context.Background()
	return SetContextWithId(c, Generator.TrackId(c))
}

// 根据已有的TrackId生成新的Track对象
func NewWithId(trackId string) context.Context {
	return SetContextWithId(context.Background(), trackId)
}

// 根据已有的Context生成新的Track对象
func NewWithContext(c context.Context) context.Context {
	if c == nil {
		c = context.Background()
	}
	return SetContextWithId(c, Generator.TrackId(c))
}

// 向已有Context中添加TrackID
func SetContextWithId(c context.Context, trackId string) context.Context {
	if c == nil {
		c = context.Background()
	}
	return context.WithValue(c, KEY_ID, trackId)
}

// 获取日志ID
func GetId(c context.Context) string {
	if c == nil {
		return ""
	}
	if trackId, ok := c.Value(KEY_ID).(string); ok {
		return trackId
	} else {
		return ""
	}
}
