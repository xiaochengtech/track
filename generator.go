/*
   Copyright 2020 XiaochengTech

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package track

import (
	"context"
	"fmt"
	"time"
)

// TrackId的生成器
type GeneratorInterface interface {
	TrackId(c context.Context) string
}

// 全局使用的生成器实例
var Generator GeneratorInterface = DefaultGenerator{}

// 默认生成器，21位纳秒时间值，年份为2位
type DefaultGenerator struct {
}

func (DefaultGenerator) TrackId(c context.Context) string {
	now := time.Now()
	return fmt.Sprintf("%s%09d", now.Format("060102150405"), now.UnixNano()%1e9)
}
