# track

这是一个为全局日志追踪设计的库，使用Go自带的`context.Context`作为上下文传递的信息载体，在内部加入TrackId的传递。

使用方法为直接调用`context.go`中的函数。支持如下情况的生成Context的方法：

* 全新的上下文。
* 已有TrackId。
* 已有Context(不含TrackId)。
* 已有Context(不含TrackId)，及一个TrackId。

还支持从Context中读取TrackId。

默认采用21位的纳秒时间值(年份仅后两位)，可以如下方式来重写TrackId的生成机制：

```go
type CustomGenerator struct {}

func (CustomGenerator) TrackId(c context.Context) string {
    // ...
    return result
}
```
